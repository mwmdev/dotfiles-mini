#!/bin/bash

#{{{ General settings

# Confirm before overwriting
alias cp="cp -i"
alias mv="mv -i"
alias ln="ln -i"

# Human readable sizes
alias df='df -h'
alias free='free -m'

# Nicer ls
alias ls='ls --color=auto'
alias ll='ls -lhA'
alias lls='ls -lhAS'
alias llt='ls -lhAt'

# Path
alias mpat='echo -e ${PATH//:/\\n}'

# Package management
alias mins='sudo apt-get install'
alias mupd='sudo apt-get update'
alias mrmv='sudo apt-get remove'

# Reload config files
alias br='. ~/.bashrc'
alias brp='. ~/.bash_profile'

#}}}

#{{{ Applications shortcuts

# File browser
alias f='vifm'

# Text editor
alias v='vim'

# Git
alias ga='git add -A'
alias gc='git commit -m'
alias gs='git status'
alias gsm='git status -uno'
alias gd='git diff'
alias gdn='git diff --name-only'
alias gpm='git push -u origin master'
alias gpb='git push -u origin bugfix'
alias gpd='git push -u origin dev'
alias gf='git fetch'
alias gpu='git pull'

# Network
alias mpig='ping -c 5 www.google.com'
alias mgip='curl ipinfo.io/'

# Services start / stop / status / restart
alias status='systemctl status'
alias restart='systemctl restart'
alias start='systemctl start'
alias stop='systemctl stop'

#}}}

#{{{ Config files shortcuts

alias mbrc='$EDITOR ~/.bashrc'
alias mbpr='$EDITOR ~/.bash_profile'
alias mbal='$EDITOR ~/.bash_aliases'
alias mlal='$EDITOR ~/.bash_local_aliases'
alias mvrc='$EDITOR ~/.vimrc'

#}}}

#{{{ Local aliases

if [ -f ~/.bash_local_aliases ]; then
  . ~/.bash_local_aliases
fi

#}}}
