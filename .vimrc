"{{{ General settings

" Disable vi compatibility
set nocompatible

" Disable filetype (reenabled later)
filetype off

" Keep 100 lines of command line history
set history=100

" Encoding
set encoding=utf-8

" Auto reload vimrc when editing it
autocmd! bufwritepost .vimrc source ~/.vimrc

" Set vim working directory to currenty opened file
set autochdir

" Disable sound
" set visualbell

" Redraw the screen only when necessary
set lazyredraw

" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype plugin indent on

" Automatically delete tralling whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Set backup directory
set backupdir=~/tmp

"}}}

"{{{ Plugins

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Plugin manager
" https://github.com/VundleVim/Vundle.vim
Plugin 'VundleVim/Vundle.vim'

" Color theme
" https://github.com/lifepillar/vim-gruvbox8
Plugin 'lifepillar/vim-gruvbox8'

" Finder
" https://github.com/junegunn
" https://github.com/junegunn/fzf.vim
" https://github.com/junegunn/fzf/wiki/examples
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
" --column: Show column number
" --line-number: Show line number
" --no-heading: Do not show file headings in results
" --fixed-strings: Search term as a literal string
" --ignore-case: Case insensitive search
" --no-ignore: Do not respect .gitignore, etc...
" --hidden: Search hidden files and folders
" --follow: Follow symlinks
" --glob: Additional conditions for search (in this case ignore everything in
"  the .git/ folder)
" --color: Search color options
command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)

" Commenting / Uncommenting blocks of code
" https://github.com/tomtom/tcomment_vim
Plugin 'tomtom/tcomment_vim'

" File explorer
" https://github.com/scrooloose/nerdtree
Plugin 'scrooloose/nerdtree'

" Snippets
" https://github.com/SirVer/ultisnips 
" Plugin 'SirVer/ultisnips'

" Git wrapper
" https://github.com/tpope/vim-fugitive
Plugin 'tpope/vim-fugitive'

" Git gutter
" https://github.com/airblade/vim-gitgutter
Plugin 'airblade/vim-gitgutter'

call vundle#end()

" }}}

"{{{ Look and feel

" Show line numbers
set number

" Show relative line numbers
" set relativenumber

" Highlight the current line
set cursorline

" Show syntac highlighting
syntax on

" Color scheme
set background=dark
colorscheme gruvbox8
hi Normal guibg=NONE ctermbg=NONE

" Add a bit extra margin to the left
set foldcolumn=0

" Set line height
set linespace=8

" Auto indentation
set autoindent

" Insert spaces when pressing tab
set expandtab

" Number of visual spaces per tab
set tabstop=2

" Number of spaces to use for each step of (auto)indent
set shiftwidth=2

" Number of spaces inserted when tab is pressed in insert mode
set softtabstop=2

" Disabls automatic commenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Make tabs and trailing spaces visible when requested
set listchars=tab:←→,trail:·,eol:↓

" Set terminal windows height
" set termwinsize=10x0

" Hides tabs
set tabline=0

" Status line
" https://dustri.org/b/lightweight-and-sexy-status-bar-in-vim.html
set laststatus=2
set statusline=
set statusline+=%#DiffAdd#%{(mode()=='n')?'\ \ NORMAL\ ':''}
set statusline+=%#DiffChange#%{(mode()=='i')?'\ \ INSERT\ ':''}
set statusline+=%#DiffDelete#%{(mode()=='r')?'\ \ RPLACE\ ':''}
set statusline+=%#Cursor#%{(mode()=='v')?'\ \ VISUAL\ ':''}
set statusline+=\ %n\                                               " buffer number
set statusline+=%#Visual#                                           " color
set statusline+=%{&paste?'\ PASTE\ ':''}
set statusline+=%{&spell?'\ SPELL\ ':''}
set statusline+=%#CursorIM#                                         " color
set statusline+=%R                                                  " readonly flag
set statusline+=%M                                                  " modified [+] flag
set statusline+=%#Cursor#                                           " color
set statusline+=%#CursorLine#                                       " color
set statusline+=\ %t\                                               " short file name
set statusline+=%=                                                  " right align
set statusline+=%#CursorLine#                                       " color
set statusline+=\ %Y\                                               " file type
set statusline+=%#CursorIM#                                         " color
set statusline+=\ %3l:%-2c\                                         " line + column
set statusline+=%#Cursor#                                           " color
set statusline+=\ %3p%%\                                            " percentage

"}}}

"{{{ Searching

" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

" Enable autocompletion
set wildmode=longest,list,full
set wildmenu

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search matches
set hlsearch

" Search as characters are entered (incremental search)
set incsearch

" }}}

"{{{ Navigation

" Open splits on the bottom and right
set splitbelow
set splitright

" Folding
set foldenable
set foldlevel=0
set foldnestmax=10
set foldmethod=marker
set modelines=1

"}}}

"{{{ Functions

function! ToggleGUICruft()
  if &guioptions=='i'
    exec('set guioptions=imTrL')
  else
    exec('set guioptions=i')
  endif
endfunction

function! WPDebugLog()
  :tabnew
  find debug.log
  ggvGdd
  :w
  :set autoread
endfunction

function! CodeFormat()

  " Remove all empty lines
  :v/./d

  " Indent all file
  :normal gg=G

  " Add spaces around = in common HTML attibutes
  exec('%s/id="/id = "/g')
  exec('%s/class="/class = "/g')
  exec('%s/title="/title = "/g')
  exec('%s/alt="/alt = "/g')
  exec('%s/src="/src = "/g')
  exec('%s/href="/href = "/g')
  exec('%s/aria-hidden="/aria-hidden = "/g')

  exec("%s/id='/id = '/g")
  exec("%s/class='/class = '/g")
  exec("%s/title='/title = '/g")
  exec("%s/alt='/alt = '/g")
  exec("%s/src='/src = '/g")
  exec("%s/href='/href = '/g")
  exec("%s/aria-hidden='/aria-hidden = '/g")

  " Add spaces around =>
  exec('%s/\s\@<!=>\+\s\@!/ \0 /g')

  " Add spaces around ->
  exec('%s/\s\@<!->\+\s\@!/ \0 /g')

  " Add space between " and >
  exec('%s/">/" >/g')

  " Add space between ' and >
  exec("%s/'>/' >/g")

  " Add space between ( and $
  exec('%s/(\$/( \$/g')

  " Add space between if and (
  exec('%s/if(/if (/g')

  " Add space between ( and !
  exec('%s/(!/( !/g')

  " Add space between ( and "
  exec('%s/("/( "/g')

  " Add space between ( and '
  exec("%s/(\'/( \'/g")

  " Add space between ( and _
  exec('%s/(_/( _/g')

  " Add space between ' and )
  exec("%s/')/' )/g")

  " Add space between ((
  exec('%s/((/( (/g')

  " Add space between ))
  exec('%s/))/) )/g')

  " Add space after ',
  exec("%s/','/', '/g")

  " Add space after ",
  exec('%s/","/", "/g')

  " Delete redundant spaces
  exec('%s/\(\S\)\s\+/\1 /g')

  " Put curly braces back at the end of function declaration
  exec(':%s/)\s*\n{/){/g')

  " Go back to top
  gg

endfunction

map <F11> <Esc>:call ToggleGUICruft()<cr>

" by default, hide gui menus
set guioptions=i

"}}}

"{{{ Keymaps

" Set the mapleader key
let mapleader = ","

" Move to the next sentence with tab
nnoremap <TAB> )

" Open a terminal
nnoremap <leader>t :term<CR>

" Exit terminal
" tnoremap <Esc> <C-\><C-n>

" Toggle Nerdtree
nnoremap <leader>o :NERDTreeToggle<CR>

" Do a diff of the opened files
nnoremap <leader>d :windo diffthis<CR><c-w>h

" Close the diff
nnoremap <leader>dd :diffof<CR>

" CodeFormat()
nnoremap <silent> <leader>cf :call CodeFormat()<CR>

" WPDebugLog()
nnoremap <silent> <leader>dw :call WPDebugLog()<CR>

" Edit snippets for current file type
nnoremap <leader>es :UltiSnipsEdit<CR>

" Git status
nnoremap <leader>gs :Gstatus<CR>

" Git diff
nnoremap <leader>gd :Gdiff<CR>

" Git add current file
nnoremap <leader>ga :Gwrite %<CR>

" Git commit
nnoremap <leader>gc :Gcommit<CR>

" Git push
nnoremap <leader>gp :Gpush<CR>

" Toggle FZF
" Depends on the FZF plugin
nnoremap <leader>f :FZF /<CR>

" Ripgrep
nnoremap <leader>r :Find<SPACE>

" List buffers
" Depends on the FZF plugin
nnoremap <leader>b :Buffers<CR>

" Yank the complete file
nnoremap <leader>ya gg0vG$y

" Exit insert mode
inoremap jk <esc>

" Toggle 80 chars column
nnoremap <leader>co :execute "set colorcolumn=" . (&colorcolumn == "" ? "80" : "")<CR>

" Toggle tabs and trailing spaces visibility
nnoremap <leader>ts :set nolist!<CR>

" New vertical split
nnoremap <c-v> :vsp<CR>

" Switch splits
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" Resize current buffer by +/- 5
nnoremap <c-left> :vertical resize -5<cr>
nnoremap <c-down> :resize +5<cr>
nnoremap <c-up> :resize -5<cr>
nnoremap <c-right> :vertical resize +5<cr>

" New tab
nnoremap <C-t> :tabnew<cr>

" Turn off search result highlighting
nnoremap <space><space> :noh<cr>

" Switch to light theme
nnoremap <leader>lt :set background=light<cr>

" Switch to dark theme
nnoremap <leader>dt :set background=dark<cr>

" Toggle indent guides
nnoremap <leader>ig :IndentGuidesToggle<cr>

" Jump to comma and insert carriage return
nnoremap <leader>j f,a<cr><esc>

" nnoremap <silent><Leader>L :put! =printf('console.log(''%s:'',  %s);', expand('<cword>'), expand('<cword>'))<CR>
nnoremap <Leader>L "ayiwOerror_log(print_r('<C-R>a:' . $<C-R>a, true));<Esc>
"}}}

"{{{ Abbreviations

abb _@ mike@mwm-webdesign.com
abb _mw Michael Wassmer
abb _lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.


"}}}

" Format closed folds line
set foldtext=v:folddashes.substitute(getline(v:foldstart),'/\\*\\\|\\*/\\\|{{{\\d\\=','','g')

" vim:foldmethod=marker:foldlevel=0
